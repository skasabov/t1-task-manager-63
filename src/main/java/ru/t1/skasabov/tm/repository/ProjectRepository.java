package ru.t1.skasabov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    private ProjectRepository() {
    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("DEMO"));
        add(new Project("TEST"));
        add(new Project("MEGA"));
        add(new Project("BETA"));
    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @NotNull
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        TaskRepository.getInstance().removeByProjectId(id);
        projects.remove(id);
    }

}
